
NeuraSim's Guide
====================================
Project to accelerate the resolution of the Poisson step in CFD solvers, by means of convolutional neural networks.
This project combines the PhiFlow v2 CFD solver with the Fluidnet_cxx_2 neural models developped by DAEP.

Description
^^^^^^^^^^^
This project aims to change the world. The history of fluid mechanics and its presence in human lives dates back to ancient Greece. From Archimedes works to the ones from the Alexandrian school. Slowly yet steadily, we started mastering the science and engineering of fluids. Which in turn, together with the other classical fields such as solid mechanics, thermodynamics, and electromagnetism, had indeed constituted the driving force of change and improvement in human society. The world we live in today owns its form to those four disciplines of knowledge. Since, using them, we calculate and design the invention that makes our lives easier. 
And it's here that arises the problem with fluid mechanics. While the others have been pretty much solved numerically. For fluids, there is still no satisfactory computational solution. For those who have work with CFD, this will come as no surprise. Even today, it might be a better solution to use potential flow for some aerodynamic problems than raw fluid mechanics. Due to its accuracy and time consumption. In some cases, it's the only option. And this puts on hold better news designs.
The goal of this project then is to solve those problems. By completely reimagining the computational techniques used to solve fluid mechanics problems. Breaking free of the present restriction to what we can simulate, and design. Allowing an explosion of new and much better designs. And, then, change the world.



Installation
============
In the following pages, you will find the basic setting up instructions to start working with NeuraSim.

.. toctree::
   :maxdepth: 2
   :caption: Installation Contests':

   Requirements
   Setup
   


Usage and Guides
================
In those pages, you will find the basic usage guides and some getting started tutorials. For more details of the workings of the software please refers to the documentation.

.. toctree::
   :maxdepth: 2
   :caption: Usage Contents':

   Interface
   Tutorial



Documentation
================
In those pages, you will find in detail information of the different functions and software structure.

[NOTICE: please notice that since it is an on going project this may be left blank until more definitive code]

.. toctree::
   :maxdepth: 2
   :caption: Documentation Contents':

   Functions
   Folders
   Reports



Contributors & Acknowledgements
===============================

20xx-2021 -- Ekhi Ajuria (Fluidnet_cxx_2 & NeuraSim)

20xx-2021 -- Michaël Bauerheim (supervision and guidance of all projects)

2021-2021 -- Antonio Giménez Nadal (development of NeuraSim)

20xx-2020 -- Antonio Alguacil (working on Fluidnet_cxx_2)
